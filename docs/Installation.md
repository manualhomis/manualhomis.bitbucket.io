**<span style="color:Black;font-size:1.5em">System Installation</span>**

**<span style="color:Black;font-size:1.4em">1. Windows Operation Systems</span>**

The installation process is as follows;

1.	Install XAMPP
2.	Install browser e.g chrome
3.  Install notepad ++
4.	Install composer
5.	Copy folder containing GoT-HoMIS V3 and paste it to htdocs C:\xampp\htdocs\got_homis_v3
6.	Open terminal/CMD
    + Change directory to root  i.e cd /
    + Then go to xampp/mysql/bin

![](img/cmdone.jpg)

<p align="justify">System will show the loading bar with progress on the top of the page. After finishing the activation system will show “SYSTEM DABABASE ACTIVATED”.</p>

**<span style="color:Black;font-size:1.3em">Create Database</span>**

![](img/cmdtwo.jpg)

 7. Make sure xampp is turned on
 8. <p align="justify">Go to htdocs, open the folder containing the database, go to .env file and edit it with notepad++, then change the database name to ensure it is similar to the name of a database you created.</p>
 9. Configure and clear cache. The cache is cleared and configured as follows;

![] (img/cache.jpg)

**<span style="color:Black;font-size:1.3em"> Database Migration and Seeding</span>**

+ Database Migration is the process of creating the database schema on the database name created in step (vi) above.
+ Database seeding is a simple way to add initial data sets into your database.

Database Migration steps:

![](img/migration.jpg)

**<span style="color:Black;font-size:1.3em">Port Assigning and Virtual Host Declaration:</span>**

If apache cannot be started on the default port (80), assign new port numbers as follows:
Open the folder; C:\xampp\apache\conf

Under config, open httpd.config with Notepad C++ then add or edit port numbers under the Port Listening configuration part.

For example 

![](img/assignport.jpg)

Then open folder …..Extra:  C:\xampp\apache\conf\extra

For example

![](img/virtualhost.jpg) 

Line 36 - 42 - Virtual Host with listening port number 7070.

Line 37:     - Administrator Server email address.

Line 38:     - Folder name where system file for the domain name are Stored.

Line 39:     - Local IP Address of the server where the system will be launched.

Line 40:      - Contains a record of critical errors that occurred during the   
              Server operation.

Line 41:    - Create custom domains for development in XAMPP.

<p align="justify"><span style = "color:red">Restart apache and make sure the port numbers you have assigned are visible on the Apache Service port numbers on the XAMPP panel</span></p>




