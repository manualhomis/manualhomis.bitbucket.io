** <span style="color:Black;font-size:1.5em">USER MATRIX</span>**

** <span style="color:Black;font-size:1.5em">GoT-HoMIS Basic Setup Duties</span>**
<p align="justify">This Document provides an overview of various duties that are needed to make the system operational. The duties specified here are those that happens once or repeatedly to allow other users of the system to work flawlessly.</p>

** <span style="color:Black;font-size:1.4em">ICT Officers Duties</span>**
<p align="justify">
i.	Make sure the system infrastructure is available and conducive

ii.	Carry out System Installation, Updating and Upgrading

iii.User registration and role assignment

<span style = "color:black">
<p align="justify">**NB: Users registered in item 3 should be an approved list provided by the Medical Officer Incharge (MOI) or the Health Secretary (HS) of the Health Facility. This is to ensure that only legitimate users are registered into the system. Should a user abdicate his/her duties, the created account must be deactivated.</p>**</span>

iv.	 Carryout Training and Supporting to system users 

v.	 Provide close assistance to users as need arises.</p>

<span style = "color:black"><p align="justify">**NOTE: Although the ICT officer is supposed to know how each part of the system functions, it is not recommended to perform duties on behalf of the assigned users. To create ownership of the entered records, the right user must be responsible in creating the record. In an event that the user needs assistance, the ICT officer should assist the user in which case the corresponding user must have been logged in into the system and the duties performed in assistance by the ICT officer and not otherwise.</p>**</span>

** <span style="color:Black;font-size:1.4em">Main Pharmacy Duties</span>**
<p align="justify">
i.	Create stores lists and grant store access to responsible personnel

ii.	Record items received to main store from MSD or Other Vendors

iii.Create issuing records to sub-stores/departments

<span style="color:black"><p align="justify">**Note: All internal requisitions (requests from any department to either MAIN STORE or SUB STORE) must be through the system and not otherwise except external requisition (requests from other HFs)</p>**</span>

iv.	Make sure all commodities at the facility have been registered and has selling prices</p>

** <span style="color:Black;font-size:1.4em">Lab/Radiology Manager Duties</span>
**
<p align="justify">
i.	Lab services setup (register tests and set prices)

ii.	Lab equipment registration and equipment status update

iii.Mapping of equipment vs tests

iv.	Allocation of Lab Technicians within corresponding sub departments</p>

** <span style="color:Black;font-size:1.4em">Theatre Incharge or Manager Duties</span>**
<p align="justify">

i.	Register all procedures performed at the Facility

ii.	Set and update price details of configured procedures</p>

** <span style="color:Black;font-size:1.4em">Ward Matron/Patron Duties</span>**
<p align="justify">

i.	Register Facility’s Wards

ii.	Register available beds in wards

iii.Perform allocation of nurses vs Wards</p>

