**<span style="color:Black;font-size:1.5em">System Requirents</span>**

<p align="justify">GoT-HoMIS requires a few software on a server machine for it to operate smoothly. Although the system can be installed and run on a normal desktop/laptop computer with decent specifications, a Health Facility shall require a “server” dedicated to host the system in the facility Local Area Network (LAN). </p>

**<span style="color:Black;font-size:1.4em">1. Windows Operation Systems</span>**

**<span style="color:Black;font-size:1.2em">Server Software requirements</span>**

- Modern Server Operating system
- Web Server:- Apache
- PHP Engine
- MySQL Database (This and the above two can all be installed once by the  XAMPP/  LAMPP package)
- An editor on Windows platform (Recommended Notepad++)
- PHP Dependence Manager (Composer)

 **<span style="color:Black;font-size:1.2em">Server Hardware Requirements</span>**

- RAM at least 8GB
- Hard disk at least 1 Terabyte
- Internet Connection

**<span style="color:Black;font-size:1.2em">Client Software Requirements</span>**

<p align="justify">Since GoT-HoMIS is a web based software, the only software required on the client is an up to date Chrome Browser.</p>

**<span style="color:Black;font-size:1.2em">Client Hardware Requirements</span>**

- At least 2GB RAM on Clients computers
- Receipt Thermal printer (Preferably Epson …) 
- (Small sticker) Barcode printer
- Barcode reader

**<span style="color:Black;font-size:1.4em">2. Unix  Operation Systems</span>**

**<span style="color:Black;font-size:1.2em">Server Software requirements</span>**

- Modern Server Operating system
- Web Server:- Apache
- PHP Engine
- MySQL Database (This and the above two can all be installed once by the  XAMPP/LAMPP package)
- An editor on Windows platform (Recommended Notepad++)
- PHP Dependence Manager (Composer)

 **<span style="color:Black;font-size:1.2em">Server Hardware Requirements</span>**

- RAM at least 8GB
- Hard disk at least 1 Terabyte
- Internet Connection

**<span style="color:Black;font-size:1.2em">Client Software Requirements</span>**

<p align="justify">Since GoT-HoMIS is a web based software, the only software required on the client is an up to date Chrome Browser.</p>

**<span style="color:Black;font-size:1.2em">Client Hardware Requirements</span>**

- At least 2GB RAM on Clients computers
- Receipt Thermal printer (Preferably Epson …) 
- (Small sticker) Barcode printer
- Barcode reader

**<span style="color:Black;font-size:1.4em">3. Linux Operation Systems</span>**

**<span style="color:Black;font-size:1.2em">Server Software requirements</span>**

- Modern Server Operating system
- Web Server:- Apache
- PHP Engine
- MySQL Database (This and the above two can all be installed once by the  XAMPP/LAMPP package)
- An editor on Windows platform (Recommended Notepad++)
- PHP Dependence Manager (Composer)

 **<span style="color:Black;font-size:1.2em">Server Hardware Requirements</span>**

- RAM at least 8GB
- Hard disk at least 1 Terabyte
- Internet Connection

**<span style="color:Black;font-size:1.2em">Client Software Requirements</span>**

<p align="justify">Since GoT-HoMIS is a web based software, the only software required on the client is an up to date Chrome Browser.</p>

**<span style="color:Black;font-size:1.2em">Client Hardware Requirements</span>**

- At least 2GB RAM on Clients computers
- Receipt Thermal printer (Preferably Epson …) 
- (Small sticker) Barcode printer
- Barcode reader

**<span style="color:Black;font-size:1.4em">4. Mac Operation Systems</span>**

**<span style="color:Black;font-size:1.2em">Server Software requirements</span>**

- Modern Server Operating system
- Web Server:- Apache
- PHP Engine
- MySQL Database (This and the above two can all be installed once by the  XAMPP/LAMPP package)
- An editor on Windows platform (Recommended Notepad++)
- PHP Dependence Manager (Composer)

 **<span style="color:Black;font-size:1.2em">Server Hardware Requirements</span>**

- RAM at least 8GB
- Hard disk at least 1 Terabyte
- Internet Connection

**<span style="color:Black;font-size:1.2em">Client Software Requirements</span>**

<p align="justify">Since GoT-HoMIS is a web based software, the only software required on the client is an up to date Chrome Browser.</p>

**<span style="color:Black;font-size:1.2em">Client Hardware Requirements</span>**

- At least 2GB RAM on Clients computers
- Receipt Thermal printer (Preferably Epson …) 
- (Small sticker) Barcode printer
- Barcode reader