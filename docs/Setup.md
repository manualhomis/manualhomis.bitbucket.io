** <span style="color:Black;font-size:1.5em">System Configuration</span>**

**<span style="color:Black;font-size:1.5em">Introduction to (GoT – HoMIS) Modules</span>**

<p align="justify">A module is a software component or part of a program that contains one or more routines. One or more independently developed modules make up a complete program. Modules are categorized according to the functionalities. User role can be assigned/removed to a user according to the user privileges.</p>

**<span style="color:Black;font-size:1.5em">Administrator</span>**

<p align="justify">Super user/super admin is the person in charge of all other users in the system. By default, the user who signs up first and creates the organization is the Super Administrator. The Super Administrator has the utmost privileges across the entire organization. Super user is capable of: Change the role of a User to Admin and vice versa. View, manage or Change the details for any organization. There can be only one Super Admin in the entire organization.</p>

**<span style="color:Black;font-size:1.2em">Administrator Login</span>**

<p align="justify">Administrator must login in to the system with the valid credentials. By Default username = admin@tamisemi.go.tz and password = 12345678.</p>

![](img/administratorlogin.jpg).

**<span style="color:Black;font-size:1.2em">System Activation</span>**

<p align="justify">After login successfully Administrator activate system Views. System views are MySQL views which control different logics and process in the system. To activate system views click on DB Setup then open system activation Tab, on system activation Tab click the button ACTIVATE. As shown below.</p>

<span style = "color:red">**Note: System database activation is compulsory. It is a very important step because it enable item sales and user details account to be activated**</span>

Open DB_Setup

![](img/dbsetup.jpg)

**<span style="color:Black;font-size:1.2em">Activate View</span>**

![](img/activateview.jpg)

System will show the loading bar with progress on the top of the page. After finishing the activation system will show “SYSTEM DABABASE ACTIVATED”.

<span style = "color:red">**NOTE: System views must be activated only once.**</span>

**<span style="color:Black;font-size:1.2em">Create Facility</span>**

For the facility to be formulated, the administrator must do the following;

1. Open the DB Setup 
2. Click on facility registration tab and register the facility details: Facility code, name, Address, email, phone number, facility type, council,region and Reattendance
<p align="justify">

<span style = "color:red">**NOTE:Reattendance is used
to limit the number of days in which the patient does not have to pay, in those days the scheduled arrival of the hospital will be registered and selected the services he/she needs but will not be paid, the patient/client will go directly to the doctor.**</span>

</p>
 
To make those settings

1. Go to DB setup

2. Open Reattendance

3. Enter a number of apatient days a patient can revisit without paying consultation fees 

![](img/facilityregistration.jpg)

**<span style="color:Black;font-size:1.2em">Reattendance</span>**

<p align="justify">This sub section is working to limit the number of days in which the patient does not have to pay, in those days the scheduled arrival of the hospital will be registered and selected the services he/she needs but will not be paid, will go directly to the doctor.</p>

<span style = "color:red">**NB:This part will be used to limit the number of days in which the patient does not have to pay especially for stations that have already begun to use the system **</span>


To make those settings

1. Go to DB setup

2. Open Reattendance

3. Enter a number of apatient days a patient can revisit without paying consultation fees 
![](img/reattendance.jpg)

After creating the Facility, Super admin register admin of a facility.


**<span style="color:Black;font-size:1.2em">Facility Admin Registration</span>** 

1. Open DB Setup
2. Click register user (enter required details)
3. Register

![](img/facilityadmin.jpg)

After registration of a facility admin, facility admin will login to the system by entering the username and password

 <span style = color:red> Note: after login user must change the password from default password to another password before using the system.</span style>

  Facility admin is responsible for creating other facility users, giving them permission user and permission roles

**<span style="color:Black;font-size:1.5em">User Registration</span>**

   1. Open DB setup
   2. click register user (enter required details)
   3. Save

![](img/userregistration.jpg)

<p align="justify">After registration, the user will be able to log in to the system but he/she won’t be able to do any task. For a user to be able to perform any task, permission must be granted by the administrator under permission user tab. Administrator will search the name of a registered user among the listed registered users and after selecting the user, admin grants the user with permission according to that user’s privilege by double click a required permission. After double click a permission those permissions displays at the right hand side under the access given tab.</p>

If a wrong permission is added it can be removed by clicking the Delete tab.

![](img/permissionuser.jpg)

<p align="justify">The system is capable of giving a permission user the ability to perform tasks of another user, the system ensure that permitted user has the required roles. Example a system can enable receptionist to perform tasks of a cashier. Administrator will do that by opening the permission role tab and selecting the system role. After selecting the targeted system role, administrator will grant the required permission which will display at the right hand side under the access given tab.
If a wrong permission is added it can be removed by clicking the Delete tab.</p>

![](img/permissionrole.jpg)

**<span style="color:Black;font-size:1.5em">Payment Category Setup</span>**

<p align="justify">Payment category is a module responsible for setting the payment sub-categories from User fee, Insurance or Exemption categories. After registering the payment subcategories, those payments will be listed in the patient payment category list.
</p>

If it hregister patient payment category do the following;

1. Open payment category setup
2. Click patient category 
3. Select patient category name 
4. Enter payment sub category.Then click register

![](img/paymentcategorysetup.jpg)

After registration of payment sub category, all the payment sub categories will be listed on patient category list.

**<span style="color:Black;font-size:1.5em">Item Setup</span>**

![](img/itemsetup.jpg)

Item setup enables users to register all items which are available in a facility. 
Item setup consist of;
**_Item price_,_Registration_,_Change item category_,_Item Category_,_SErvice config,_Exemption status_,_sub department_,_Reception Service_,_Diagnosis Registry_**

**<span style="color:Black;font-size:1.2em">Item Registration</span>**

1. enter the item name 
2. choose item department.
3. Click register

![](img/itemregistration.jpg)+

**<span style="color:Black;font-size:1.2em">Item Sub department Registry</span>** 

1. Search the item
2. choose sub department Registry of that item.
3. Register


![](img/subdepartmentregistry.jpg)

**<span style="color:Black;font-size:1.2em">Item Sub department list</span>** 

<p align="justify">After items registration the registered items with their corresponding sub departments are listed on this sub module. on this part the system has the edit and delete options.</p>

**<span style="color:Black;font-size:1.2em">Item List</span>**

All registered items with the corresponding departments are listed here

![](img/itemlist.jpg)

**<span style="color:Black;font-size:1.2em">Item Price</span>** 

Item price consist of;*Create item price* and *Item price list*

**<span style="color:Black;font-size:1.2em">Create item price</span>**

In this sub module,the price of all items are set according to the payment categories.

1. Search the item
2. Enter the price of items according to the payment category. 

![](img/itemprice.jpg)

**<span style="color:Black;font-size:1.2em">Item price list</span>**

The created items with their prices are listed on item priced list.
![](img/itempricelist.jpg)

**<span style="color:Black;font-size:1.2em">Change Item Category</span>**

In this sub module,Items assigned with incorrect categories are edited here by searching an item and choosing its correct category.

![](img/changeitemcategory.jpg)

**<span style="color:Black;font-size:1.2em">Item category</span>**

under this sub section the registered items are listed where the system gives an option to edit/delete the item.

![](img/itemcategory.jpg)

**<span style="color:Black;font-size:1.5em">Service Config</span>**

Service configuration is a section in which all patients categories are defined. It consist of three parts namely;

 1. Exemption settings
 2. One time(papo kwa papo)payments settings 
 3. Insurance settings.

 <span style = "color:red">**N.B: A POINT TO NOTE**</span>

<p align="justify">For Facilities which has a fixed or limited CHF bill per visit(per day). For Example KIOMBOI District Hospital which has a limited Bill Amount For CHF Beneficiaries per Day which was 20,000 Tsh which is paid by the CHF Insurance.If it happens that the amount exceeds 20,000, The exceeded amount will be paid by the CHF Client from his/her pocket. 
Therefore:, This Part of CHF setting has focused only to those facilities which are having such scenario mention above, 
Otherwise NO need to Do any of this CHF setting Described below.</p>

Below are the steps showing the CHF setting;

1. Go to your system folder and open this path **"app/classes"**
2. Open **CHF_settings.php**
3. it will look like the image below

![](img/chf1.jpg)

4. Set
 ["use_chf_setting" **to 1** and
 "chf_ceiling" to **Your Amount**  
]
as shown below

![](img/chf2.jpg)

**<span style="color:Black;font-size:1.2em">Exemption Settings</span>**

<p align="justify">Exemption as its name suggests is a service for patients who do not pay for services offered during their stay in the health facility,although there are few services that are restricted to exemption.
Those restricted services needs to be set in order to let the system know which items has no exemption.</p>

<span style = "color:red">**N.B: By default all items has exemption.**</span>

Steps to set items which has no exemption(these items do not have exemption):

1. Check Exemption button
2. search items
3. Click No Exemption button **to set items which has no exemption**

Steps to set items which has exemption (these items have exemption)

1. Check Exemption Button
2. search items
3. Click Set Exemption button **to reset items into exemption**

![](img/exemptionsettings.jpg)

**<span style="color:Black;font-size:1.2em">One Time Payment</span>**

<p align="justify">Is a service in the respective health facility in which patients pay only once during their stay in the health facility. 

Example; Patients pays only for consultation and the rest of the services are offered for free.

Those services which are given to patients for free, are NOT totally free. At the end of the day, the cost of those services needs to be known. In order to archieve this, the price for all items(Services, Medicine and Medical supplies) needs to be set then the system needs to be told which items or services will be paid and which items will not be paid meaning that they will be offered for free to patients.</p>

Steps for one time payment

1. Check One time button
2. Search item
3. Click one time payment to set item which will not be billed once consultation has been paid

Steps for Normal Payment

1. Check One time button
2. Search item
3. Click normal payment to set item which will  be billed once ordered.
![](img/onetimepayment.jpg)

**<span style="color:Black;font-size:1.2em">Insurance Settings</span>**

<p align="justify">Normally patients with insurance do not pay for services when they go for medical examination.

although they do not pay for services,there are services which are not paid by Insurance institution.</p>

To set those items do the following

To set item which is not paid by respective insurance

 1. Check insurance button
 2. Search item
 3. Tick appropriate insurance category
 4. Click Not beneficiary to set items which is Not paid by respective insurance

 To set item which will be paid by respective insurance

 1. Check insurance button
 2.  Search item
 3. Tick appropriate insurance category
 4. Click beneficiary to set items which will be paid by respective insurance

![](img/insurance.jpg)

**<span style="color:Black;font-size:1.5em">Sub Department</span>**

<p align="justify">This sub module enables sub departments to be registered and after registration the registered sub departments are listed at the right side of the page.
Sub departments are important for measuring performance and for report preparation.</p>

![](img/subdepartmentregistration.jpg)

**<span style="color:Black;font-size:1.5em">Reception Services</span>**

<p align="justify">It gives an option to search and to save services that needs to be viewed by the receptionist during patient/client registration.</p>

![](img/receptionservices.jpg)

**<span style="color:Black;font-size:1.5em">Diagnosis Registry</span>**

<p align="justify">This part allows the registration of diagnosis and diagnosis code. After registration these diagnosis are viewed by the doctor and the doctor is able to assign these diagnosis to a patient.</p>

**<span style="color:Black;font-size:1.5em">Pharmacy</span>**

<p align="justify">Pharmacy is a module in a system which is responsible for managing and dispensing drugs. It is the module which focus on safe and effective medication use. The system allow Pharmacist to inform patients in all aspects of their medicine including recommending types as well as administration route and dosages. Hospital pharmacist are responsible for monitoring the supply of all medicines used in the hospital and are in charge of purchasing, dispensing and quality testing their medication stock.
Pharmacy contains sub modules such as</p>

1. Item setup
2. Main pharmacy
3. Sub store 
4. Dispensing

**<span style="color:Black;font-size:1.3em">Main Store (Main Pharmacy)</span>**

Main pharmacy consist of, setting Requisitions, Item record, receiving and issuing.

![](img/mainpharmacy.jpg)

**<span style="color:Black;font-size:1.2em">Setting</span>**

In this sub module the system is capable of creating store, vendors, invoices, store access, Tracer medicine and Transaction types

**<span style="color:Black;font-size:1.2em">Create New Store</span>**

1. click create a new store
2. Enter store name
3. Select store type
4. click Register for new store registration.
![](img/storecreation.jpg)

**<span style="color:Black;font-size:1.2em">Create New Vendor</span>** 

1. Click Create a new vendor
2. Enter the required details i.e; Vendor name, phone number,address and contact person
3. click Register for new Vendor registration.
![](img/vendorcreation.jpg)

**<span style="color:Black;font-size:1.2em">Create New Invoice</span>**

1. click create a new Vendor
2. Enter invoice number
3. Select Vendor name

**<span style="color:Black;font-size:1.2em">Store Access</span>**

Store access are given to responsible users of a certain store by;

1. Search user 
2. Assign user to a store access
3. save

![](img/storeaccess.jpg)

**<span style="color:Black;font-size:1.2em">Tracer Medicine</span>** 

This part allows the tracer medicine to be registered by;

1. searching the specific items to be traced 
2. select the item status the status of that items.(status can be whether the item is available or not available in the facility.) 
3. if there is more than one item, add all items to list
4. Register
![](img/tracermedicine.jpg)

**<span style="color:Black;font-size:1.2em">Transaction Type</span>**

All transaction types used in receiving and issuing are registered here.

1. Enter transaction type name
2. Select adjustment type(adjustment can be positive or negative)

![](img/transactiontype.jpg)

**<span style="color:Black;font-size:1.2em">Mark P.O.S Dispensing</span>**


**<span style="color:Black;font-size:1.2em">Setting summary</span>** 

This part lists store list,vendor list and invoice list.
![](img/settingsummary.jpg)
3.7 Lab Setting
<p align="justify">Is a module in the system which is used for registration of lab items. Lab setting consist of; Equipments, lab tests,test prices, allocate test,configurations and reports</p>
![](img/lab.jpg)

**<span style="color:Black;font-size:1.5em">Equipments</span>** 

This sub module consist of Equipments registration, Equipment list and

**<span style="color:Black;font-size:1.2em">Equipments Registration</span>** 

<p align="justify">Under this sub module is where the lab equipment’s are registered with the equipment status, reagent to be used and the sub department where that investigation will be carried. The equipment status is the determinants of whether the test will be taken or not if the status of an equipment is: Equipment/Reagent is OK, test can be done that means the test will be done but if the status is: Equipment has malfunctioned, test cannot be done, that means the test cannot be done.</p>

To register equipment;

1.Click Equipment

2.Then Equipment registration

3.Enter equipment name, status,reagent to be used and sub-department

4.Save

![](img/equipment.jpg)

**<span style="color:Black;font-size:1.2em">Equipments list</span>**

<p align="justify">Is the list of all registered equipments containing the Equipment name,equipment description and the equipment status(ON/OFF)which shows whether the test can be done or not.</p>

![](img/equipmentlist.jpg)
 
**<span style="color:Black;font-size:1.2em">Lab Tests</span>** 

<p align="justify">This part is where the test panel and its components as well as the single test are registered. Test panel is the group of tests all taken by a single equipment while single test is a test taken by a single equipment. N.B. The component of panel can be registered as a single test</p>

**<span style="color:Black;font-size:1.2em">Tests Pannel Registration</span>**  

To register tests panel

1.Enter panel name

2.Select the equipment

3.Save

![](img/panelregistration.jpg)

**<span style="color:Black;font-size:1.2em">Reallocate Lab Sections</span>**  

Components registration

To register a component

1.Search the test panel

2.Enter the component name

3.Enter the Minimum and Maximum units

4.Save

![](img/componentsregistration.jpg)

**<span style="color:Black;font-size:1.2em">Singel Test Registrations</span>**   

To register a single test

1.Enter test name

2.Search equipment

3.Save

![](img/singletestregistration.jpg)

**<span style="color:Black;font-size:1.2em">Registered Tests</span>** 

This sub section contains a list of registered tests
To see the registered tests;

1.Search the equipment

2.Click get test list

<p align="justify">All registered tests will be displayed showing the Lab test name , equipment name and equipment status and options, where on equipment option there is an option of changing the machine to be used by clicking on change machine and selecting the required equipment then click apply.</p>
![](img/registeredtests.jpg)

**<span style="color:Black;font-size:1.2em">Set Tests Off</span>**

This subsection is used to set a test OFF.when a test is set OFF that means a test is present but cannot be done.
![](img/settestsoff.jpg)

**<span style="color:Black;font-size:1.2em">Set Tests On</span>**

This subsection is used to set a test ON.  when a test is set ON that means a test is present but can be done.
![](img/settestsON.jpg)

**<span style="color:Black;font-size:1.2em">Test Prices</span>**

This part enable a user to set prices of the registered lab tests. 
Prices are set according to the facility user payment categories.

1.Search for a lab test

2.Enter prices of that lab test according to the payment categories

3. Specify the dates

4.Save
![](img/testprice.jpg)

**<span style="color:Black;font-size:1.2em">Allocate Test</span>**

This sub module allow the registered lab test to be searched, assigned an equipment and a test category.
![](img/allocatetest.jpg

**<span style="color:Black;font-size:1.2em">Configurations</span>**

This sub-section consist of staff allocation, Quick LabTest settings and activate default machine.

**<span style="color:Black;font-size:1.2em">Staff Allocation</span>**

It is where the access is given to a user responsible to view all the lab tests sent to that sub-department.
To allocate staff

1.Click sub-department
![](img/configurations.jpg)

2.Search lab technologist
![](img/configurations1.jpg)

![](img/technologistlist.jpg)
3.Save

**<span style="color:Black;font-size:1.5em">Ward Management</span>** 

<p align="justify">Ward management is a module responsible for the management of a hospital ward.</p> 
<p align="justify">It is a part of the In-Patient Module and is used to set wards, to locate beds and to assign nurse to ward.</p>
![](img/wardmanagement.jpg)

**<span style="color:Black;font-size:1.2em">Create Ward</span>**

To create ward follow the following steps;

1. Go to item setup register Wrd class and assign it a price
2. Go to Ward management
3. Click ward type
4. Enter ward type then click Register
![](img/wardtype.jpg)
5. Enter ward grade then click Register
![](img/wardgrade.jpg)
6. Create ward by entering the ward name, then select ward type and ward class then Save (N.B you can also create ward through ward setup icon found in wards)
![](img/createward.jpg)
7. To see the list of all created wards click wards
![](img/wards.jpg)

**<span style="color:Black;font-size:1.2em">Create Bed</span>** 

i. Click wards

ii.Click view beds

iii. Enter bed name then select bed type

iv. Save

v. All registered beds will be listed on bed list
![](img/bedlist.jpg)

8. Go to Accomodation fee in order to set price to a ward as follows;

i.   Click Accomodation fee(to enter the item price) 

ii.  Search for any ward grade to set price

iii. Enter the prices of each payment category

iv.  Enter the time span of those prices

v.  Save

![](img/accomodationfee.jpg)

**<span style="color:Black;font-size:1.2em">Nurse Allocation</span>**

Nurse allocation tab is where nurse is assigned the access to ward

i. Click nurse allocation

ii.Go to nurse assignment

iii.choose ward name and nurse name
          
iv. Click allocate to assign nurse to a ward
![](img/nurseallocation.jpg)
v. Allocated nurse will be listed on nurse list by searching the nurse name.
![](img/nurselist.jpg)

**<span style="color:Black;font-size:1.5em">Care and Treatment Clinic (CTC)</span>**

<p align="justify">Care and Treatment Clinic (CTC) is a clinic which deals with HIV/AIDS infected people. It explains on how the HIV patient is monitored from medical supply to consultation. CTC covers all areas of medicine, consultation and all other related treatments including counseling and voluntary testing, also education oh how to care for People living with HIV/AIDS (PLWHA) CTC contains:
Setup, Codes, ARV and TB, Discussion Topics </p>

**<span style="color:Black;font-size:1.2em">Clinic Setup</span>**
![](img/clinicsetup.jpg)
 In this sub-section;

 1.Enter clinical capacity per day where you write the total number of patients who can be treated that day, the facility CTC number and health facility file number.


**<span style="color:Black;font-size:1.2em">Codes</span>**

Click on Codes then enter codes and codes descriptions
![](img/codes.jpg)

**<span style="color:Black;font-size:1.2em">ARV & TB</span>**

On each item in this sub-section the registration is performed by entering codes and descriptions of that code
![](img/arvtb.jpg)

**<span style="color:Black;font-size:1.5em">Social Welfare</span>**

<p align="justify">Are various social services provided by a state for the benefit of its citizens across the hospital. The major roles of these welfare officers are assisting patients cope with their condition and hospital environment and identify a social problem which affects their health. Other responsibilities include counselling, reconciliation, exemption, patient’s follow-up through home visiting and advice.</p>

![](img/socialwelfare.jpg)
 
 Social welfare consist of setup, Reports,others,referral issues,GBV/VAC,change patient category and Registration.

**<span style="color:Black;font-size:1.2em">Set up</span>**

This sub module ie responsible for the configurations,it consists of ; Exemption acess, GBV/VAC types, Violence types, Social issues register, Violence services and Violence output.

**<span style="color:Black;font-size:1.2em">Exemption Access</span>**

This sub section is responsible for provision of exemption access to a user.

1. search user
2. Assign exemption access to that user.

![](img/exemptionaccess.jpg).

**<span style="color:Black;font-size:1.2em">GBV/VAC Types</span>**

Basically there are only two types of violance which are Gender Based Violence (GBV) and Violence against Child.

To register GBV/VAC type

1. Enter violance name
2. Register

![](img/gbvvactypes.jpg)

**<span style="color:Black;font-size:1.2em">Violence Types</span>**

To register violence type

1. Choose the violance name.
2. Enter the the violance type name.
3. Add the items to list
4. Save

![](img/violencetypes.jpg)

**<span style="color:Black;font-size:1.2em">Social Issue Register</span>** 

To register social issue;

1. Enter name of the issue
2. Save

![](img/socialissue.jpg)

**<span style="color:Black;font-size:1.2em">Violence Types</span>**

These are services provided due to occurance of various violance.

To register Violence services ;

1.Enter servive name

2.Add serive name to list

3.save

![](img/Violenceservices.jpg)

**<span style="color:Black;font-size:1.2em">Violence Output</span>**

Violence outputs are outcome of violence occurances. Violence output can be Physical disabilities, to get infection,physical health disorder.

To register Violence output;

1.Enter violence output
2.Add to list
3.Save

![](img/Violenceoutput.jpg)

**<span style="color:Black;font-size:1.5em">GBV/VAC</span>**

<p align="justify">Under GBV/VAC sub section the system allows user to search for violence that patient encounted, service is given, client violence effect is analysed, informant details are recorded and if there is any attachment,the attachment will be uploaded.</p>

GBV/VAC consist of ;

**<span style="color:Black;font-size:1.2em">Violence</span>**

To register the encountered violance;

1.Search violence type 
2. Enter date
3. Search violence category
4. Tick where appropriate
5. Save

![](img/violance.jpg)

**<span style="color:Black;font-size:1.5em">Emergency Registration</span>** 

<p align="justify">This part deals with the registration process for patients that present to the Emergency Department.To ensure accurate capture of information as well as to ensure proper patient care.sometimes Obtaining the correct information on patients upon arrival is critical as this information may not be available due to the patient health condition,
In this case a patient can be registered with unknown or estimated details. later on the correct details will be entered when the patient condition is better.</p>

![](img/emergencyregistration.jpg)

Emergency department consist of 

i. Emergency setup (orange button at the bottom right) 

ii. Casuality reception

iii. Updating information

iv. Emergency reports

**<span style="color:Black;font-size:1.2em">Emergency Setup</span>**

 1. Select the emergency type
 2. Enter emergency name
 3. Register

![](img/emergencysetup.jpg)

**<span style="color:Black;font-size:1.5em">Hospital Shop</span>**

<p align="justify">The hospital shop is where medicine and medical supplies are sold. The hospital shop is used where the item does not exist in the hospital dispensing or when an outside customer comes out to buy something from the hospital shop.</p>
![](img/hospitalshop.jpg)

To set hospital shop do the following;

1. Go to item setup and set prices of all the items available to the Hospital shop for a payment category of hospital shop.

2. Issue those hospital shop priced items to the hospital shop dispensing

3. Give access to the user dealing with the dispensing services to the Hospital shop dispensing and make sure the cashier of dispensing is given to the hospital's module

![](img/hospitalshop1.jpg)

**<span style="color:Black;font-size:1.5em">CREATING AUTO BACKUP</span>**
<p align="justify">Create a new file with .bat extension so as to write backup scripts example: ups.bat</p>

Write these scripts and then save file.

![](img/ab1.jpg)

@echo off

	set vardate="%date%"

	set vartime="%time%"

C:\xampp\mysql\bin\mysqldump -uroot --password= gothomis > "E:\backup\-%vardate%-%vartime%.sql" 

1.	Change date format to the format that does not include { / }

![](img/ab2.jpg)    ![](img/ab3.jpg)




