**<span style="color:Black;font-size:1.5em">User Guide</span>**

**<span style="color:Black;font-size:1.5em">Login</span>**

<p align="justify">The system provides the user with default Username and Password.After the first login, the system allows the user to change default Password by choosing password of their choises.</p>

**<span style="color:Black;font-size:1.2em">Login into GoT-HoMIS</span>**

1. Open the browser and type the IP address followed by full colon (:) then port number
for example <span style = "color:red"> 127.0.0.1:7070 </span>

**<span style="color:Black;font-size:1.2em">Login Page</span>**

![](img/loginpage.jpg)

**<span style="color:Black;font-size:1.5em">Reception</span>**

<p align="justify">This is the fundamental module of the Hospital Management System. Every Patient visiting the hospital for outpatient service or inpatient service is first registered using this module. The module captures the basic personal details of the patient .under registration the system generates the Unique Sequential Medical Record Number (MRN), which is used as a patient identity, wherever he/she goes the medical records can be viewed for both outpatient as well as inpatient services. The system also generates Sequential Account Number (SAN) which is generated every time a patient pays a visit to the facility; SAN controls every execution a patient will undergo.  Patients can be classified as user fee, Exemption or Insurance.</p>

![](img/reception.jpg)

Reception consist of;

1. Patient registration 
2. Corpse registration
3. Insurance verification
4. Reports
5. Change Patient Category
6. Edit Patient

**<span style="color:Black;font-size:1.2em">Patient Registration</span>**

<p align="justify">The system has two options of patient/client registration. Registration can be quick or full registration. Quick registration is useful for time saving where receptionist will enter only few details of a patient and the remaining details will prompt be filled on the patient next visit.</p>
Full registration involves entering full details of a patient.

To register patient do the following;

1. Enter patient details

![](img/patientregistration.jpg)

2. After registering a patient, receptionist choose patients payment category.

![](img/patientpaymentcategory.jpg)

Payments categories include 

i. User fee

ii. Exemption

iii. Insurance

3.After assigning the patient payment category,choose the clinic or service where the patient will be consulted by the doctor then click save.

![](img/chooseclinic.jpg)

<p align="justify">After choosing the clinic then save, the system will automatically generate the patient card.A card contains the facility name and patient particulars.
The card carries the Unique Sequential Medical Record Number (MRN), which is used as a patient identity, wherever he/she goes</p>

**<span style="color:Black;font-size:1.2em">Insurance</span>**

This part consist of;

i. Card verification 

ii. Card mapping to patient.

**<span style="color:Black;font-size:1.1em">Card Verification</span>**

This part enable the patients under insurance category to be registered by ;

1. Select Insurance type
2. Enter membership number
3. Select Visit type
4. Verify

![](img/cardverification.jpg)

NHIF will verify the patient details and automatically NHIF register the patient. This helps the facility to send the correct payment details of a patient.

![](img/authorizationnumber.jpg)

5. Save

<p align="justify">The system will automatically generate the patient card.A card contains the facility name and patient particulars.
The card carries the Unique Sequential Medical Record Number (MRN), which is used as a patient identity, wherever he/she goes.</p>

![](img/insurancecard.jpg)

**<span style="color:Black;font-size:1.1em">Card Mapping to Patient</span>**

<p align="justify">There are patients who are not under any insurance category. but later they join and become parts of insuarance, to map those patients with the insurance card do the following.</p>

1. Search patient re-attendance
2. Enter card number
![](img/patientcard.jpg)

**<span style="color:Black;font-size:1.2em">Change Patient Category</span>**

<p align="justify">To change the patient category from one payment category to another payment category.</p>

1. Search patient to change category
2. Choose a correct option you want to convert to the patient
3. Save changes

you will be prompted with a message asking if you are sure with the change

![](img/changecategoryreg.jpg)

**<span style="color:Black;font-size:1.2em">Corpse Registration</span>**

<p align="justify">All corps from outside the facility are registered under this sub category.When registering corpse several details are entered such as ;Taarifa za marehemu, Taarifa za aliyeleta maiti,Mali za marehemu,Taarifa za mtoa maiti,Hariri taarifa za maiti.</p> 

Taarifa za marehemu

![](img/taarifazamarehemu.jpg)

A message will pop up in order to complete the information (kamilisha taarifa za aliyeleta maiti).

![](img/taarifazamletamaiti.jpg)

After registering a corpse, receptionist choose payment category. payment category can be billing or exemption payment

![](img/corpsebilling.jpg)

Mali za marehemu

Taarifa za mtoa maiti

![](img/mtoamaiti.jpg)

Hariri taarifa za maiti

This sub-section deals with updating corpses information. whenever wrong data were submitted during registration of the corpse can  be corrected in that section. 


<span style = "color:red">_**N.B: Patient Registration is of two types;
                             Normal Registration or Emergency Registration.**_</span>

**<span style="color:Black;font-size:1.5em">Emergency Registration</span>**

<p align="justify">This part deals with the registration process for patients that present to the Emergency Department.To ensure accurate capture of information as well as to ensure proper patient care.sometimes Obtaining the correct information on patients upon arrival is critical as this information may not be available due to the patient health condition,
In this case a patient can be registered with unknown or estimated details. later on the correct details will be entered when the patient condition is better.</p>

![](img/emergencyregistration.jpg)

**<span style="color:Black;font-size:1.2em">Casuality Reception</span>**

It is where the patient registration is conducted.There is a search button which allows user to search for re-attendance patients.

to register emergency patient do the following

1. Open Casuality reception
2. Enter patient details
3. click quick register

![](img/casualreception.jpg)

4. After registering a patient, receptionist choose patients payment category. Either Billing or exemption payment.

5. For billing patient ,Choose payment category,type of emergency, service and clinic

6. Enter encounter.

![](img/exemptionpayment.jpg)

7.the system will automatically generate the patient card.A card contains the  patient particulars.

![](img/patientcard.jpg)

8. Make payments

9. Go to Resuscination room to enter the Emergency Department.

![](img/resuscination.jpg)

In emergency department there are;

**<span style="color:Black;font-size:1.2em">Consultation Queue</span>**

<p align="justify">Consultation queue contains a list of patients who have satisfied to be consulted by the doctor which involves patients who have made payments and exempted patients all of which are waiting to be consulted by the doctor concerned, there is a quick search tab that will enable the doctor to search a patient by patient’s full name and his/her medical record number.</p>
Doctor picks the clients/patients from consultation queue in order to attend them as follows:

![](img/consultationqueue.jpg)

Mtuha tallying
![](img/mtuhatallying.jpg)

**<span style="color:Black;font-size:1.1em">Previous Visit</span>**

<p align="justify">First the doctor looks at the patient’s previous visits history. For the new patients, the previous visits history will be seen once the patient is clerked by the doctor.</p>
If it is a new patient, there won’t be previous visit history until the patient is clerked is when the patient history will be available

![](img/emergencypreviousvisit.jpg)

**<span style="color:Black;font-size:1.1em">Clerk Sheet</span>**

<p align="justify">This is where the patient emergency type is found. In clerk sheet the doctor discuss with the patient about the illness. After discussing the doctor will be able to search the complaints and duration of the occurrence of that complaints according to what the patient said apart from searching the complaints, the system enable doctor to write the complaints in details from what was heard from the patient.</p>

![](img/clerksheet.jpg)

**<span style="color:Black;font-size:1.1em">Diagnosis</span>**

<p align="justify">After clerking a patient, this part enables a doctor to write the possible diagnosis, the possible diagnosis written here will be confirmed by the doctor once the investigation are complete and investigation results are sent to the doctor.</p>

![](img/diagnosis.jpg)

**<span style="color:Black;font-size:1.1em">Investigation</span>**

This part enables a doctor to choose what a patient is to be investigated. After choosing the investigation the patient will make payments and go for investigation.

![](img/investigation.jpg)

**<span style="color:Black;font-size:1.1em">Investigation Results</span>**

<p align="justify">Results of what had been investigated from the Laboratory/Radiology is returned to the doctor, the doctor views the investigation results, the results will assist doctor to confirm diagnosis and make treatment.</p>

![](img/investigationresults.jpg)

**<span style="color:Black;font-size:1.1em">Treatment</span>**

<p align="justify">This part enable a doctor to give medication, medical supplies or procedures to a patient. Doctor will be able to see all prescribed medications, previous procedures and rejected prescription.</p>

**<span style="color:Black;font-size:1.1em">Blood Request</span>**

It is where the patient need for blood is requested and the request goes to the blood bank.

![](img/bloodrequest.jpg)

![](img/treatment.jpg)

**<span style="color:Black;font-size:1.1em">Disposition</span>**

<p align="justify">On disposition a patient can be admitted to the ward, transferred to other clinic, referred to external hospital or deceased to mortuary.</p>
On admission, the doctor admits the patient by writing the admission note and select a ward

![](img/disposition.jpg) 

**<span style="color:Black;font-size:1.2em">Treatment Queue</span>**

<p align="justify">Treatment queue contains a list of patients who have already gone through the laboratory for investigation and they are waiting to get diagnosis confirmation from the doctor so as to receive treatment. Diagnosis is confirmed under diagnosis sub module and medication is provided on treatment.</p>

![](img/treatmentqueue.jpg)

**<span style="color:Black;font-size:1.2em">Death Certification</span>**

<p align="justify">Death certification involves a list of corpse who are coming outside the facility and have been registered by receptionist and they await to be certified by the doctor, this certification allows the doctor to fill in the immediate cause of death of a particular corpse and underlying conditions for that death in order to certify the death.</p>

![](img/deathcertification.jpg)

**<span style="color:Black;font-size:1.2em">My Report</span>**

 This sub category shows the casuality minor procedure(the report shows the casuality mainor procedures) and casuality accident report.

 ![](img/report.jpg)

**<span style="color:Black;font-size:1.2em">Performance</span>**

This section gives out the performance evaluation report of a particular doctor depending on a specific date and time.

![](img/myperformance.jpg)

**<span style="color:Black;font-size:1.5em">Debtor</span>**

This section contains Debtor,Debtor list, Discount and Debt summary report.Under this sub section there are two things ivovled;

First a Debtor client is created, then the created clients will be listed on debtor list and on Debt summary .

Secondly those clients on debtor list will have to pay for those Debts and the payments starts once the system initiates debt clearance.

![](img/debtor.jpg)

![](img/debtorone.jpg)

**<span style="color:Black;font-size:1.2em">To create debtor and Debtor list</span>**

To create debtor

1. Click debtor
2. Open  discount
3. Type MRN to search client
4. Click create debt(to include that patient in list of clients who will make payment later on)

![](img/cleardebt.jpg)

All clients created as debtors will be listed on debtor list

![](img/debtorlist.jpg)

**<span style="color:Black;font-size:1.2em">Initiate Clear Debt</span>**

To initiate debt cleareance (to clear bill)

1. Click debt list
2. Click view to initiate debt clearance
3. Check box to all items which bills are to be cleared
4. Save to change transaction record to normal payment

![](img/initiatecleardebt.jpg)

**<span style="color:Black;font-size:1.2em">Debt Summary Report</span>** 

This sub section summaries the debtors report.

![](img/debtreport.jpg)


**<span style="color:Black;font-size:1.5em">Social Welfare</span>**

 <p align="justify">Are various social services provided by a state for the benefit of its citizens across the hospital. The major roles of these welfare officers are assisting patients cope with their condition and hospital environment and identify a social problem which affects their health. Other responsibilities include counselling, reconciliation, exemption, patient’s follow-up through home visiting and advice.</p>

![](img/socialwelfare.jpg)
 
 Social welfare consist of setup, Reports,others,referral issues,GBV/VAC,change patient category and Registration.

**<span style="color:Black;font-size:1.2em">Registration</span>**

Under this sub section it is where registration of a patient is done.There is a quick search button which allows user to search for a patient who is already registered.

![](img/registrationsocial.jpg)

**<span style="color:Black;font-size:1.2em">Reports</span>**

This sub module allows social welfare to view various types of reports including;

**<span style="color:Black;font-size:1.2em">Exemption List</span>** 

This report lists all the exemptions provided,, exempton details together with the name of the one who gave those exemptions.

![](img/exemptionlist.jpg)

<span style = "color:red">**N.B:  If a patient needs to be changed from one exemption type to another exemption type (Example from Temporary Exemption to Above 60) or if a patient needs to be changed from Exepmtion to Billing (Exapmle from Under five to self referral) do the following**</span>

1. Click reports
2. Open exemption list
3. Search patient
4. Tick edit
5. Go to Exemption type, then select the required type

![](img/changeexemption.jpg)

**<span style="color:Black;font-size:1.2em">Exemption Summary</span>** 

Gives a summary report of gender, exemption type given and the number of those who got that exemption.

![](img/exemptionsummary.jpg)

**<span style="color:Black;font-size:1.2em">Employee Summary</span>** 

 This report shows the employees performance, it shows how many people were given exemption by that employee in a given period of time.

 ![](img/employeesummary.jpg)

**<span style="color:Black;font-size:1.2em">Finance Summary</span>**

This gives the overall finance summary report for total number of all exemptions from all departments.

![](img/financesummary.jpg)

**<span style="color:Black;font-size:1.5em">Bills Payments</span>**

<p align="justify">This module is used to record the charges for the patient services provided by the various clinical departments, e.g. lab tests, medication, medical supply, procedures and consultation. Payments are done electronically through the use of Government electronic Payment Gateway (GePG), Cash payment or POS.</p>

For cash payment; the system Process payment request and generate receipt

![](img/billpayment.jpg)

![](img/billreceipt.jpg)

<p align="justify">For GePG the system first generates the control number which is used for payment, after payment a notification message is sent to the patient and to the facility to show that payment is complete and successful.</p>

![](img/gepg.jpg)

Bills payment sub-module enable user to check balance of cash and GePG transactions.
![](img/checkbalanceopd.jpg)

**<span style="color:Black;font-size:1.5em">Point of Sale</span>**

<p align="justify">Point of sale is an electronic equipment performing the sales transaction and processing the credit card payments.</p>

<p align="justify">Point of sale is applied when a patient comes from prescription straight to the payment point, the cashier enters required items manually to the system. The client is provided with prescribed item after payment and automatically the system generates receipt.</p>

<p align="justify">POS is used for facilities with partially network system configured. POS is also useful for payment issues to the facilities which face the temporary network problems.</p>

Steps to complete POS

1. Open POS
2. Search patient
3. Seach item
4. Add item
5. Process sale

POS enable user to check balance of the processed transactions

![](img/pointofsale.jpg)

**<span style="color:Black;font-size:1.2em">Partial Payment</span>**

<p align="justify">Is a module that is used when a patient/client needs to make payment but the patient has insufficient amount of money.</p>
<p align="justify">The system allows that partial amount to be processed and the system will automatically generate receipt.</p>

![](img/partialpayment.jpg)

The system allows a client to be searched from search box in order to view a list of all items for discount, there after commit partial payment 

![](img/partialpayment1.jpg)

**<span style="color:Black;font-size:1.5em">Vital Signs</span>**

<p align="justify">Vital signs are a group of most important signs that indicate the status of the body’s vital functions, these measurements are taken to help assess the general physical health of a person, give clues to possible disease, and show progress toward recovery.</p>

![](img/vitalsigntaking.jpg)

Select patient then register vitals

![](img/registervitals.jpg)

![](img/vitals.jpg)

**<span style="color:Black;font-size:1.5em">Out Patient(OPD)</span>**

**<span style="color:Black;font-size:1.4em">Consultation Queue</span>**

<p align="justify">Consultation queue contains a list of patients who have satisfied to be consulted by the doctor which involves patients who have made payments and exempted patients all of which are waiting to be consulted by the doctor concerned it also contains a quick search tab that will enable the doctor to search a patient by patient’s full name and his/her medical record number.</p>
Doctor picks the clients/patients from consultation in order to attend them as well as for MTUHA tallying:
![](img/consultationqueueopd.jpg)
Check the corresponding patient detail to enable MTUHA tallying.
![](img/mtuhaopd.jpg)

**<span style="color:Black;font-size:1.2em">Previous Visit</span>**
<p align="justify">First the doctor looks at the patient’s previous visits history. For the new patients, the previous visits history will be seen once the patient is clerked by the doctor.
If it is a new patient, there won’t be previous visit history until the patient is clerked is when the patient history will be available<p/>

![](img/previousvisitopd.jpg)

**<span style="color:Black;font-size:1.2em">Vital Sign</span>**

This part enables a doctor to view all vitals that were taken concerning a particular patient.

**<span style="color:Black;font-size:1.2em">Clerk Sheet</span>**

<p align="justify">This is where a doctor discuss with the patient about the illness. After discussion, the doctor will be able to search the complaints and duration of the occurrence of that complaints according to what the patient said apart from searching the complaints, the system enable doctor to write other complaints in details as it was heard from the patient.</p> 
![](img/complainsopd.jpg)

<p align="justify">Doctor will write the history of presenting illness and take a review of other systems to check if there is any inconsistency compared to the given explanations.</p>
![](img/historyillnessopd.jpg)

**<span style="color:Black;font-size:1.1em">Review of Other Systems</span>**

<p align="justify">This sub section is used by the healthcare providers for producing a medical history from a patient. It is often structured as a component of an admission note covering the organ systems, with a focus upon the subjective symptoms perceived by the patient (as opposed to the objective signs perceived by the clinician). Along with the physical examination, it can be particularly useful in identifying conditions that do not have precise diagnostic tests.</p>
![](img/reviewsystemopd.jpg)

<p align="justify">Past medical and surgical history is the information gained by a physician by asking specific questions, either of the patient or of other people who know the person and can give suitable information with the aim of obtaining information useful in formulating a diagnosis and providing medical care to the patient.The doctor will write any past medical and surgical history of that patient and be able to search for any allergy to that patient. If it happens that a patient is allergic to something, the allergy message will pop up to the treatment tab to remind the doctor when medication is prepared to be given to that patient.</p>
![](img/medicalsurgicalopd.jpg)

**<span style="color:Black;font-size:1.1em">General Examination</span>**

<p align="justify">The process by which a medical professional investigates the overall body of a patient from head to toes for signs of disease ,the process of examining the whole body of a patient involves a physical touch.</p>
![](img/generalexaminationopd.jpg)

**<span style="color:Black;font-size:1.1em">Local Examination</span>**

<p align="justify">The process by which a medical professional investigates the specific part of the body of a patient,the process of examining the whole body of a patient involves a physical touch.</p>
![](img/localexaminationopd.jpg)

**<span style="color:Black;font-size:1.1em">Systemic Examination</span>**

<p align="justify">Systemic examination reviews the major systems of the body like the Central nervous system, Respiratory system, Cardiovascular system and Gastrointestinal system.</p>
Generally, there are 4 parts of physical examination:

● Inspection: Looking for signs

● Palpation: Feeling for signs

● Percussion: Tapping for signs, used when doing a lung, heart or gut 
  examination.

● Auscultation: Listening for sounds within the body using a stethoscope, or 
  in olden times, purely listening with direct ear.

Systemic examination is currently divided into 4 sub-modules/segments:

● Central Nervous System

● Respiratory System

● Cardiovascular System

● Abdomen Examination
The doctor needs to do only those activities relevant for the given virtual case to diagnose correctly.

![](img/systemicexamination.jpg)

**<span style="color:Black;font-size:1.2em">Diagnosis</span>**

<p align="justify">Is the process of determining which disease or condition explains a person's symptoms and signs.</p>
<p align="justify">A differential diagnosis is the distinguishing of a particular disease or condition from others that present similar clinical features. It is essentially a list of diseases/disorders that could be the cause of symptoms a person is having, and the signs that they do.</p>
<p align="justify">A provisional diagnosis is one to which the clinician is not yet committed.based on the information he has, he is making an educated guess about the most likely diagnosis</p>

![](img/diagnosisopd.jpg)

**<span style="color:Black;font-size:1.2em">Investigation</span>**

This part enables a doctor to choose the appropriate department that patients investigations needs to be taken.

1. Go to a particular department
2. Choose the required investigation
3. After choosing all investigations required from departments concerning a particular patient, those investigations will be listed at the bottom.
4. Save

![](img/investigationopd.jpg)

2.Save
![](img/staffperformance.jpg)

Sample status
![](img/samplestatus.jpg)

**<span style="color:Black;font-size:1.2em">Investigation Results</span>**

<p align="justify">Results of what had been investigated from the Laboratory/Radiology is returned to the doctor, the doctor views the investigation results, the results will assist doctor to confirm diagnosis and make treatment.</p>
![](img/investigationresultopd.jpg)

**<span style="color:Black;font-size:1.2em">Treatment</span>**

This part enable a doctor to give medication, medical supplies or procedures to a patient. Doctor will be able to see all prescribed medications, previous procedures, previous investigation and rejected prescription.

![](img/treatments.jpg)

**<span style="color:Black;font-size:1.2em">Blood Request</span>** 

It is where the patient need for blood is requested and the request goes to the blood bank.

![](img/bloodrequestopd.jpg)

**<span style="color:Black;font-size:1.2em">Disposition</span>**

On disposition a patient can be admitted to the ward, transferred to other clinic, referred to external hospital or deceased to mortuary.

On admission, the doctor admits the patient by writing the admission note and select a ward that a patient will be admitted.
![](img/dispositionopd.jpg)

**<span style="color:Black;font-size:1.3em">Treatment Queue</span>**

<p align="justify">Treatment queue contains a list of patients who have already gone through the laboratory for investigation and they are waiting to get diagnosis confirmation from the doctor so as to receive treatment. Diagnosis is confirmed under diagnosis sub module and medication is provided on treatment.</p>

![](img/treatmentqueue.jpg)

**<span style="color:Black;font-size:1.3em">Cancel Bills</span>**

This sub section enables a doctor to cancel bills of a patient. A doctor will cancel a patient bill if the bill is wrongly written and sent to a cashier or when a patient has insufficient amount of money for paying the bills.

![](img/cancelbills.jpg)

![](img/cancelbills1.jpg)

ii. Treatment queue

<p align="justify">Treatment queue contains a list of patients who have already gone through the laboratory for investigation and they are waiting to get diagnosis confirmation from the doctor so as to receive treatment. Diagnosis is confirmed under diagnosis sub module and medication is provided on treatment.</p>

![](img/treatmentqueue.jpg)

**<span style="color:Black;font-size:1.3em">Death Certification</span>** 

<p align="justify">Death certification involves a list of corpse who are coming from outside the facility and have been registered by receptionist and they await to be certified by the doctor, this certification allows the doctor to fill in the immediate cause of death of a particular corpse and underlying conditions for that death in order to certify the death.</p>

![](img/deathcertification.jpg)

**<span style="color:Black;font-size:1.3em">My Performance</span>** 

This section gives out the performance evaluation report of a particular doctor depending on a specific date and time.

![](img/myperformance.jpg)

**<span style="color:Black;font-size:1.5em">Laboratory</span>** 

**<span style="color:Black;font-size:1.2em">Lab Setting</span>**
<p align="justify">Is a module in the system which is used for registration of lab items. Lab setting consist of; Equipments, lab tests,test prices, allocate test,configurations and reports</p>
![](img/lab.jpg)

**<span style="color:Black;font-size:1.2em">Equipments</span>**
This sub module consist of Equipments registration and Equipment list

**<span style="color:Black;font-size:1.2em">Equipment Registration</span>**
<p align="justify">Under this sub module is where the lab equipment’s are registered with the equipment status, reagent to be used and the sub department where that investigation will be carried. The equipment status is the determinants of whether the test will be taken or not if the status of an equipment is: Equipment/Reagent is OK, test can be done that means the test will be done but if the status is: Equipment has malfunctioned, test cannot be done, that means the test cannot be done.</p>

To register equipment;

1.Click Equipment

2.Then Equipment registration

3.Enter equipment name, status,reagent to be used and sub-department

4.Save

![](img/equipment.jpg)

**<span style="color:Black;font-size:1.2em">Equipment List</span>**

<p align="justify">Is the list of all registered equipments containing the Equipment name,equipment description and the equipment status(ON/OFF)which shows whether the test can be done or not.</p>
![](img/equipmentlist.jpg)

**<span style="color:Black;font-size:1.2em">Re-Allocate Lab Sections</span>**

**<span style="color:Black;font-size:1.2em">Lab Tests</span>**

<p align="justify">This part is where the test panel and its components as well as the single test are registered. Test panel is the group of tests all taken by a single equipment while single test is a test taken by a single equipment. N.B. The component of panel can be registered as a single test</p>

**<span style="color:Black;font-size:1.2em">Tests Panels Registration</span>**

To register tests panel

1.Enter panel name

2.Select the equipment

3.Save

![](img/panelregistration.jpg)

**<span style="color:Black;font-size:1.2em">Components Registration</span>**

To register a component
1.Search the test panel

2.Enter the component name

3.Enter the Minimum and Maximum units

4.Save

![](img/componentsregistration.jpg)

**<span style="color:Black;font-size:1.2em">Single Test Registration</span>**

To register a single test

1.Enter test name

2.Search equipment

3.Save

![](img/singletestregistration.jpg)

**<span style="color:Black;font-size:1.2em">Registered Tests</span>**

This sub section contains a list of registered tests
To see the registered tests;

1.Search the equipment

2.Click get test list

<p align="justify">All registered tests will be displayed showing the Lab test name , equipment name and equipment status and options, where on equipment option there is an option of changing the machine to be used by clicking on change machine and selecting the required equipment then click apply.</p>
![](img/registeredtests.jpg)

**<span style="color:Black;font-size:1.2em">Set Tests OFF</span>**

This subsection is used to set a test OFF.  when a test is set OFF that means a test is present but cannot be done.
![](img/settestsoff.jpg)

**<span style="color:Black;font-size:1.2em">Set Tests ON</span>**

This subsection is used to set a test ON.  when a test is set ON that means a test is present but can be done.
![](img/settestsON.jpg)

**<span style="color:Black;font-size:1.2em">Unavailable Tests</span>**

These are tests which cannot be taken because they are not available in a particular facility.To view the unavailable tests 

1.Specify the dates

2.Click search
![](img/unavailabletests.jpg)

**<span style="color:Black;font-size:1.2em">Test Prices</span>**

This part enable a user to set prices of the registered lab tests. 
Prices are set according to the facility user payment categories.

1.Search for a lab test

2.Enter prices of that lab test according to the payment categories

3. Specify the dates

4.Save
![](img/testprice.jpg)

**<span style="color:Black;font-size:1.2em">Allocate Test</span>**

This sub module allow the registered lab test to be searched, assigned an equipment and a test category.
![](img/allocatetest.jpg

**<span style="color:Black;font-size:1.2em">Configurations</span>**

This sub-section consist of staff allocation, Quick LabTest settings and activate default machine.

**<span style="color:Black;font-size:1.2em">Staff Allocation</span>**

It is where the access is given to a user responsible to view all the lab tests sent to that sub-department.
To allocate staff

1.Click sub-department
![](img/configurations.jpg)

2.Search lab technologist
![](img/configurations1.jpg)

![](img/technologistlist.jpg)
3.Save

**<span style="color:Black;font-size:1.2em">Reports</span>**

This sub-section consist of staff performances and sample status.

Staff Performances 
This part shows staff performance report

To view staff

1.Specify the dates

2.Save
![](img/staffperformance.jpg)

Sample status
![](img/samplestatus.jpg)

**<span style="color:Black;font-size:1.5em">Collect Sample</span>**

This sub section consist of collect sample, receive sample and sample tracking
![](img/samplecollection.jpg)

**<span style="color:Black;font-size:1.2em">Samples</span>*
*
This sub-section consist of Collect sample

Collect sample
This sub-section enable patient sample to be collected.To do so collect patient sample then click sample to save.
![](img/collectsample.jpg)

The system will automatically generate barcode for patient sample
![](img/samplebarcode.jpg)

**<span style="color:Black;font-size:1.2em">Rejected Sample</span>**

All collected sample which seems not to be right are rejected so they can be collected again. under this sub section is where all rejected samples are listed
![](img/rejectedsample.jpg)

**<span style="color:Black;font-size:1.5em">Sample Testing</span>**

This sub-section consist of Sample testing, Result approve, Blood stock and reports.
![](img/sampletesting0.jpg)

**<span style="color:Black;font-size:1.2em">Sample Testing</span>**

All collected sample are listed here for testing.
 To collect sample do the following

 1.Click sample testing to view the list of patients whose sample needs to be collected
 ![](img/sampletesting.jpg)
 ![](img/sampletesting1.jpg)
 ![](img/investgationresults.jpg)

**<span style="color:Black;font-size:1.2em">Results Approve</span>**

<p align ="justify">If the investigation results are found correct they will be approved and a notification message will be sent to a patient The system allow user to reject investigation results if found incorrect. The rejected sample will go back to sample collector.</p>

1.Click result approve
 ![](img/resultapprove.jpg)
 ![](img/investigationreport.jpg)
  ![](img/resultapprove.jpg)

**<span style="color:Black;font-size:1.2em">Blood Order</span>**

  This sub-section carries the number of customers who need blood. to view and issue the requested blood;

  1.Click blood order

  2.Enter the blood group and the requested units

  3.Click issue
   ![](img/bloodorder.jpg)

**<span style="color:Black;font-size:1.2em">Blood Stock</span>**

This part consist of sub-sections such as Blood bank balance, unit issued/used, blood issuing and receiving stock.

**<span style="color:Black;font-size:1.2em">Blood Bank Balance</span>**

This sub-section stores information on the whole stock of existing blood units.
 ![](img/bloodbank.jpg)

 A graphical view of Blood bank balance
  ![](img/bloodgraphical.jpg)

**<span style="color:Black;font-size:1.2em">Unit Issued/Used</span>**

This part shows the units issued/used internal or external. A user is able to check the graphical view of the corresponding units.

1.Specify dates

2.Search
 ![](img/unitsissued.jpg)

**<span style="color:Black;font-size:1.2em">Blood Issuing</span>**

This part enable user to issue blood

1.Enter place or facility you want to issue

2.Choose blood group

3.Enter number of units to issue
 ![](img/bloodissuing.jpg)

**<span style="color:Black;font-size:1.2em">Receiving Stock</span>**

This sub-section enable user to to receive by

1.Choose blood group

2.Enter number of units

3.Add to list
 ![](img/receivingstock1.jpg)

**<span style="color:Black;font-size:1.2em">Reports</span>**

<p align="justify">this sub-section contains various reports such as user performances,sample status,preview results,specimen rejection form,TAT report,order audit trail,report results remotely and pending results.</p>

**<span style="color:Black;font-size:1.2em">Your Perfomances</span>**

This part contains user performances report
1.Specify the dates
2.Search
 ![](img/receivingstock1.jpg)

**<span style="color:Black;font-size:1.2em">Sample Status</span>**

This part shows status of the sample, the status is either sample is verified,not verified or waiting for verification.
 ![](img/samplestatuslab.jpg)

**<span style="color:Black;font-size:1.2em">Preview Results</span>**

This part enable user to preview lab results 
 ![](img/previewresults.jpg)

**<span style="color:Black;font-size:1.2em">Specimen Rejection Form</span>**

This part shows the form of rejected specimen
 ![](img/specimenrejection.jpg)

**<span style="color:Black;font-size:1.2em">TAT Report</span>**

 Turnaround Time Report provides laboratorians turnaround time with more discrete turnaround time points to measure, monitor and identify opportunities for improvement
 ![](img/TATreport.jpg)

**<span style="color:Black;font-size:1.2em">Order Audit Trail</span>**
 ![](img/audittrail.jpg)

**<span style="color:Black;font-size:1.2em">Report Results Remotely</span>**
 ![](img/resultremotely.jpg)

**<span style="color:Black;font-size:1.5em">Pharmacy</span>**

**<span style="color:Black;font-size:1.5em">Main Pharmacy</span>**

<p align="justify">Pharmacy is a module in a system which is responsible for managing and dispensing drugs. It is the module which focus on safe and effective medication use. The system allow Pharmacist to inform patients in all aspects of their medicine including recommending types as well as administration route and dosages. Hospital pharmacist are responsible for monitoring the supply of all medicines used in the hospital and are in charge of purchasing, dispensing and quality testing their medication stock.
Pharmacy contains sub modules such as</p>

**<span style="color:Black;font-size:1.2em">R&R</span>**
<p align="justify">This R&R,Report and Requisition is the paper created from the facility where all report and requisition data can be entered then entered into eLMIS for MSD ordering process</p>

**<span style="color:Black;font-size:1.2em">Requisition</span>**

<p align="justify">In requisition is where the incoming requisition can be viewed(the quick search button allows the search for a specific requisition) and new requisition is created.
The system allows all requisitions to be added on the list then the items are saved at once.</p>
![](img/createquisition.jpg)

![](img/incomingrequisition.jpg)

**<span style="color:Black;font-size:1.2em">Item Record</span>**

This sub section consist of 
1. Store records
2. Received and issued items
3. Tracer medicine reports 
4. Ledger
5. Expired items

**<span style="color:Black;font-size:1.2em">Stored Record</span>**

<p align="justify">This sub-part gives user an option to search a specific item or to view all reports of ; stored item, the store balance, detailed report, reorder level and tracer medicine.</p>

This part enable user to view the balance of stored item as shown below

![](img/storebalance.jpg)

**<span style="color:Black;font-size:1.2em">Detailed Report</span>**

Detailed report describe all details of an item

![](img/detailedreport.jpg)

**<span style="color:Black;font-size:1.2em">Received and Issued</span>** 

<p align="justify">This part has a quick search button which allows a user to search for an item. this part also has a received and issued button,by checking the button,the user is able to view the status/report of receiced or issued items.</p>
 
![](img/receivedissueditem.jpg)

**<span style="color:Black;font-size:1.2em">Dispensed Items</span>**

This part shows the report of dispensed items

**<span style="color:Black;font-size:1.2em">Tracer Medicine</span>**

This part shows the report of tracer medicine

**<span style="color:Black;font-size:1.2em">Ledger</span>**

<p align="justify">This is the principal book for recording and totaling economic transactions measured in terms of a monetary unit of an account with debits and credits in separate columns and a beginning monetary balance and ending monetary balance.</p>
![](img/ledger.jpg)

**<span style="color:Black;font-size:1.2em">Expired Item</span>**

This part shows the report of expired items

**<span style="color:Black;font-size:1.2em">Receiving & Issuing</span>**

<p align="justify">Main store receives items from vendors and issue items to sub store, dispensing or to another main store. The system gives an option to search specific items or to view all items by specifying the date, both for issued and received records.</p>

Note:  the system allows main store to issue items directly from main store to dispensing without passing to sub store.

**<span style="color:Black;font-size:1.2em">Receiving</span>**

1.On item receiving, if the item has a new invoice number, check the New invoice Box as follows;

![](img/newinvoice.jpg)

2.Then receive the items by entering all the required details

![](img/itemreceiving.jpg)

![](img/itemreceiving1.jpg)

3.Add all the received items then save. The system automatically calculate total price of all items received.

![](img/itemreceiving2.jpg)

**<span style="color:Black;font-size:1.2em">Issuing</span>**

To be able to isseu an item, that item must be received first
Steps to issue an item;

1.Click item Issuing and choose the item you want to issue.

![](img/itemissuing.jpg)

2.Fill all the required items and add items to list
3.Save

![](img/itemissuing1.jpg)

**<span style="color:Black;font-size:1.5em">Substore</span>**

Sub store consist of Items, Item receiving and requisition
![](img/substore.jpg)

**<span style="color:Black;font-size:1.2em">Items</span>**

This part lists store balance and detailed report of the received item
![](img/items.jpg)

**<span style="color:Black;font-size:1.2em">Item Issuing</span>**

This part is responsible for item issuing
to issue an item;

1. Search the item to issue
2. Enter all other details
3. Add all items to issue 
4. save(successfull issued message will pop up)

![](img/itemissuing2.jpg)

**<span style="color:Black;font-size:1.2em">Requisition</span>**

 Requisition consist of two things; Create requisition and Incoming requisition

**<span style="color:Black;font-size:1.2em">Create Reuisition</span>**

requisitions are created to mainstore, another substore or dispensing.

![](img/requisition1.jpg)

**<span style="color:Black;font-size:1.2em">Incoming Requisition</span>**

<p align="justify">All requisitions coming from other stores will be viewed on this part, there is a quick search which allows the user to search for a specific requisition.</p>

**<span style="color:Black;font-size:1.5em">Dispensing Window</span>**

<p align="justify">Dispensing is module that verifies client/patient prescriptions from doctors by either accepting it or rejecting it in case that prescription has some discrepancies. After verifying what the doctor has prescribe then the items are dispensed.</p>

![](img/dispensingwindow.jpg)
Dispensing consist of

1. Items
2. Dispensing
3. Requisition
4. Prescription verification

**<span style="color:Black;font-size:1.2em">Items</span>**

<p align="justify">This sub section lists a balance of all available items, the system enables user to search for store balance, detailed report and dispensed items.</p>

![](img/itemsdispensing.jpg)

**<span style="color:Black;font-size:1.2em">Requisition</span>**

<p align="justify">The system allows requisition to be sent from dispensing directly to main store and from dispensing to sub store as well as from one sub store to another sub store</p>

![](img/makerequisition.jpg)

**<span style="color:Black;font-size:1.2em">Prescription Verification</span>**

It is where the prescriptions are verified for payment.

![](img/prescriptionverification.jpg)

On prescription verification dashboard, enter and verify the right quantity of items to be prescribed to a patient.

Click reject if that prescription has some discrepancies,rejected goes back to the doctor for correction then dispensed again.

Click verify for a correct prescriptions in order to allow the patient to be able to make payments ready for dispensing.(once you click verify, a message will pop for verification because verification one done cannot be reverted)

![](img/prescriptionverification1.jpg)

**<span style="color:Black;font-size:1.2em">Dispensing</span>**

<p align="justify">Dispensing includes the taking in and handing out prescription where provision of drugs or medicines as set out properly on a lawful prescription.</p>
![](img/dispensing.jpg)

Enter all details to complete prescription 

![](img/dispensing1.jpg)

![](img/dispensing2.jpg)

**<span style="color:Black;font-size:1.5em">Blood Bank</span>**

<p align="justify">Blood bank is a center where blood is gathered as a result of blood donation then it is stored and preserved for later use in blood transfusion. The term "blood bank" typically refers to a division of a hospital where the storage of blood product occurs and where proper testing is performed.</p>
 ![](img/bloodbank1.jpg)

Blood bank contains sub-parts such as Requisitions, registration,stock, blood screening, collection tool, dodoso, products and non-conformaties.

**<span style="color:Black;font-size:1.2em">Stock</span>

This sub-section stores information on the whole stock of existing blood units.
 ![](img/bloodbankbalance.jpg)

 A graphical view of Blood bank balance 
 ![](img/bloodgraphicalview.jpg)

**<span style="color:Black;font-size:1.2em">Unit Issued/Used</span>** 

This part shows blood units used to patients and blood units issued to another facility. A user is able to check the graphical view of the corresponding units.

1.Specify dates

2.Search 

 ![](img/unitissued.jpg)

**<span style="color:Black;font-size:1.2em">Blood Issuing</span>**

This sub-section enable blood to be issued out of the facility

1.Check issue out of the facility

2.Search for a client

3.Enter place or facility you want to issue

4.Choose blood group

5.Enter number of units

6.Issue 
 ![](img/bloodissuing1.jpg)

**<span style="color:Black;font-size:1.2em">Receiving Stock</span>**

This sub-section enable items to be received.

1.Choose blood group

2.Enter number of units

3.Add to list 
 ![](img/receivingstock2.jpg)

**<span style="color:Black;font-size:1.2em">Requisitions</span>**

This sub-section deals with issuing blood to a patient by choosing the required blood group and units that needs to be issued to a client.
 ![](img/bloodrequisition.jpg)
 ![](img/bloodrequisition1.jpg)

**<span style="color:Black;font-size:1.2em">Registration</span>**

This sub-module is used for searching an existing client who needs to donate blood or for registration of a client who needs to donate blood also for patients who needs blood

1.Choose client/Enter client details

2.register client/continue to fill client details on blood transfusions

 ![](img/bloodregistration.jpg)

 ![](img/mchangiadamu.jpg)

**<span style="color:Black;font-size:1.2em">Blood Screening</span>**

This sub-module is intended to ensure that recipients receive the safest possible blood products.

 ![](img/bloodscreening.jpg)

**<span style="color:Black;font-size:1.2em">Screening Report</span>**

This part shows the blood screening report
![](img/screeningreport.jpg)

**<span style="color:Black;font-size:1.2em">Collection Tool</span>**

This part shows the report of blood collection data
![](img/collectiontool.jpg)

**<span style="color:Black;font-size:1.2em">Dodoso</span>**

This part shows the report of a client who donate blood
![](img/blooddodoso.jpg)

**<span style="color:Black;font-size:1.2em">Products</span>**

This part shows the report of number of blood products produced.

**<span style="color:Black;font-size:1.2em">Non Conformaties</span>**

This part contains the report of non conformities

**<span style="color:Black;font-size:1.5em">Ward Management</span>**

<p align="justify">Ward management is a module responsible for the management of a hospital ward.</p> 

<p align="justify">It is a part of the In-Patient Module and is used to set wards,create beds and to assign nurse to ward.</p>
![](img/wardmanagement.jpg)

**<span style="color:Black;font-size:1.5em">Nursing Care</span>**

<p align="justify">Nursing care is a care provided by a registered nurse for patients. This module consist of sub modules such as admission request, admitted, doctor remarks, nursing care plans,treatment,Charting, collect sample, reports and discharge.</p>
![](img/nursingcare.jpg)

**<span style="color:Black;font-size:1.2em">Admission Request</span>**

<p align="justify">Is where the list of admitted patient from the doctor will be found, the role of a nurse under admission request tab is to accept admission request and to assign bed to the admitted patient.</p>
![](img/admissionrequest.jpg)

Click admitt to assign bed to admitted patient
![](img/admittedbed.jpg)

**<span style="color:Black;font-size:1.2em">Admitted</span>**

<p align="justify">Is a list of all admitted patients, this part allows the nurse to transfer patient to another bed, to transfer patient to another ward, to decease a patient, to provide all other services ordered to a patient, to deal with absconded  patients and serious patients.</p>
![](img/admitted.jpg)

Transfer patient to another bed
Nurse will choose a bed where the patient will be transferred.
![](img/transfertoanotherbed.jpg)

Transfer patient to another ward
The nurse is able to transfer patient to another ward and write reasons for the transfer.
![](img/patienttoward.jpg)

Decease patient
![](img/deceased.jpg)

Services ordered to a patient are added and listed at this section
![](img/serviceordered.jpg)

Patient abscondee from ward
![](img/abscondee.jpg)

Serious patient
![](img/seriouspatient.jpg)

Discharge Against Medical Advice (DAMA), these are people who leaves the hospital against medical advice.
![](img/DAMA.jpg)

**<span style="color:Black;font-size:1.2em">Doctor Remarks</span>**

This sub module contains all continuation notes observed by the doctor during the major ward round and service ward round.Clinical notes marks the health status/progress of a patient.
![](img/doctorremarks.jpg)

**<span style="color:Black;font-size:1.2em">Nursing Care Plans</span>**

<p align="justify">This sub module enables a nurse to write the series of activities that guides him/her to take care of a patient. The nurse choose the date, time,nursing diagnosis,objective of care,implementation and evaluation. After writing the plan a nurse will be able to preview those plans.</p>
![](img/nursingcareplans.jpg)

![](img/nurseplan.jpg)

**<span style="color:Black;font-size:1.2em">Treatments</span>**

<p align="justify">Nurse search a patient,provide emergency drug (by entering the date, time,drug name, dose, time interval and remarks) and view the drug sheet.Drug sheet contains; patient details,the type of drug, dose, instruction, hours and other details. Then nurse will click dispense, choose the date and time that drug was given.click save.
</p>

Emergency Drug

<p align="justify">A sheet that allows a nurse in the ward to write the emergency drugs to the admitted patients,those additional emergency drugs are specified with the dosage, interval and remarks</p>

![](img/emergencydrug.jpg)

Drug sheet

<p align="justify">Drug sheet is a sheet containing drugs written by the doctor for the admitted patient</p>

![](img/drugsheet.jpg)

**<span style="color:Black;font-size:1.2em">Charting</span>** 

<p align="justify">The chart is a legal medical record, communicating crucial information to other members of the health care team so that they can make informed decisions. An accurate chart is critical to assessing the patient’s health status, and determining future care and treatment methods.</p>
![](img/charting.jpg)

**<span style="color:Black;font-size:1.2em">Input Form</span>**

<p align="justify">This is the section within charting where all ingridients given to a patients either through oral or I.V. or both oral and I.V. is recorded which are messured in (mls). That is to say all inputs that a patients takes has to be recorded by the nurse administering them for futher follow up and it's mainly applicable for patients that have been admitted.</p>
![](img/inputform.jpg)

**<span style="color:Black;font-size:1.2em">Turning Chart</span>**

<p align="justify">Here is where the nurse recordes all actions that involves positioning of the patients for the kind of patients that can not turn themselves so nurse has to change their position and keep recorde.</p>
![](img/turningchart.jpg)

**<span style="color:Black;font-size:1.2em">Output Form</span>**

<p align="justify">This subsection is where the nurse recordes the outcome from the ingridients that was given to a patient which can either be vomit or feaces and it's amount in terms of mls.</p>
![](img/outputform.jpg)

**<span style="color:Black;font-size:1.2em">Treatment Chart</span>**

 <p align="justify">This chart illustrates a range of symptoms and their respective treatment or services which monitors patients on medications for compliance and side effects.</p>
![](img/treatmentchart.jpg)

**<span style="color:Black;font-size:1.2em">Collect Sample </span>**

This sub section contains the list of patients whose sample needs to be collected as instucted by the doctor in ordered investigations. Once a patient makes payment to the ordered investigation,list of those patients with ordered investigation will be listed under this sub section for sample collection.
![](img/treatmentchart.jpg)

**<span style="color:Black;font-size:1.2em">Reports</span>**

<p align="justify">This sub section shows the admission status report. to view the admission status choose the start date and the end date then search.</p>
![](img/admissionstatus.jpg)

**<span style="color:Black;font-size:1.2em">Discharge</span>**

<p align="justify">This sub module of discharging a patient occurs when a patient is formally released after an episode of care. The reasons for discharge include finalisation of treatment, signing out against medical advice, transfer to another healthcare institution, or because of death.</p>
![](img/collectsample.jpg)

**<span style="color:Black;font-size:1.5em">Care and Treatment Clinic (CTC) </span>**

<p align="justify">Care and Treatment Clinic (CTC) is a clinic which deals with HIV/AIDS infected people. It explains on how the HIV patient is monitored from medical supply to consultation. CTC covers all areas of medicine, consultation and all other related treatments including counseling and voluntary testing, also education oh how to care for People living with HIV/AIDS (PLWHA) CTC contains:
Setup, Codes, ARV and TB, Discussion Topics </p>

**<span style="color:Black;font-size:1.2em">Previous Visit</span>**

<p align="justify">First the doctor looks at the patient’s previous visits history. For the existing patients, the previous visits history will be seen on previous visits.
If it is a new patient, there won’t be previous visit history until the patient is clerked.</p>

![](img/ctcpreviousvisit.jpg)

**<span style="color:Black;font-size:1.2em">Vital Sign</span>**

This part enables a doctor to view all vitals that were taken concerning a particular patient.

**<span style="color:Black;font-size:1.2em">Clinics</span>**

<p align="justify">This part is where doctor discuss with the patient about the illness. After discussion the doctor will be able to search the visit type and record the patient status. After checking the patient status the doctor will be able to decide whether the patient needs to be transferred to other clinics.</p>
![](img/ctcclinic.jpg)

**<span style="color:Black;font-size:1.2em">Clerk Sheet</span>**

<p align="justify">This is where doctor discuss with the patient about the illness. After discussion the doctor will be able to search the chief complaints and write the history of presenting illness.The doctor will write any past medical and surgical history of that patient and be able to search for any allergy to that patient. If it happens that a patient is allergic to something, the allergy message will pop up to the treatment tab to remind the doctor when medication is prepared to be given to that patient.</p>
![](img/ctcclerksheet.jpg)

**<span style="color:Black;font-size:1.2em">Diagnosis</span>**

<p align="justify">After clerking a patient, this part enables a doctor to write the possible diagnosis, the possible diagnosis written here will be confirmed by the doctor once the investigation are complete and investigation results are sent to the doctor.</p>
![](img/ctcdiagnosis.jpg)

**<span style="color:Black;font-size:1.2em">Investigation</span>**

<p align="justify">This part enables a doctor to write all investigations that needs to be taken to a patient </p>
![](img/ctcinvestigation.jpg)

**<span style="color:Black;font-size:1.2em">Investigation Results</span>**

<p align="justify">This sub section shows the results of what had been investigated from the Laboratory/Radiology, the investigation results are returned to the doctor, the doctor views the investigation results, the results will assist a doctor to confirm diagnosis and make treatment.</p>
![](img/ctcinvestigationresults.jpg)

**<span style="color:Black;font-size:1.2em">Treatment</span>**

<p align="justify">This part enable a doctor to gives medication, medical supplies or procedures to a patient. Doctor will be able to see all prescribed medications, previous procedures and rejected prescription.</p>
![](img/ctctreatment.jpg)

**<span style="color:Black;font-size:1.2em">Dispositiont</span>**

<p align="justify">On disposition a patient can be admitted to the ward, transferred to other clinic, referred to external hospital or deceased to mortuary.</p>
![](img/ctcdisposition.jpg)

**<span style="color:Black;font-size:1.5em">Theatre</span>**

**<span style="color:Black;font-size:1.2em">Doctor Teatre</span>**

Is a facility within a hospital where surgical operations are carried out. this sub section consist of theatre requests, reports, theatre services and findings report.
![](img/doctortheatre.jpg)
**<span style="color:Black;font-size:1.2em">Theatre Services</span>**

This sub-section enable user to search for services registration, service list and procedures
![](img/theatredoctor.jpg)

**<span style="color:Black;font-size:1.2em">Services Registration</span>** 

In service registration a user is able to search for a service, to choose procedure category and location, then register.
![](img/theatreservices.jpg)

**<span style="color:Black;font-size:1.2em">Services List</span>**

This sub-section lists all registered services
![](img/servicelist.jpg)

**<span style="color:Black;font-size:1.2em">Procedures</span>**

This sub sections shows the report of theatre procedures.

**<span style="color:Black;font-size:1.2em">Findings Report</span>**

This sub-section enable a user to search a patient and write report of what has been found after surgiary.
![](img/findingsreport.jpg)

**<span style="color:Black;font-size:1.2em">Theatre request</span>**

<p align="justify">For a theatre doctor to view patient list in theatre request,that patient has to pass through OPD to enable an OPD doctor to assign the procedures to be taken for a particular patient. </p>
![](img/theatrerequest.jpg)
![](img/theatrerequest1.jpg)

**<span style="color:Black;font-size:1.2em">Report</span>**

This subsection enable a user to view the procedures report 
![](img/procedurereport.jpg)

**<span style="color:Black;font-size:1.2em">Anaesthesia</span>**

<p align="justify">Anaesthesia is a state of temporary induced loss of sensation or awareness. This sub-section consists of prepared list, pre-anaesthetic visit, Intra operation, recovery and post-anaesthetic visit.</p>
![](img/anaesthesia.jpg)

**<span style="color:Black;font-size:1.2em">Prepared List</span>**

This sub-section consist of the the list of patients waiting for anaesthesia.
![](img/preparedlist2.jpg)
![](img/preparedlist1.jpg)

Click accept to see if the patient on the list is the informed consent and check if the relative of a patient has signed the consent form. 

**<span style="color:Black;font-size:1.2em">Pre-anaesthetic visit</span>**

This sub-section contains the list of patients for anaesthesia.
![](img/preanaesthetic.jpg)

Click record to clerk and record other details of a patient for anaesthesia then confirm for staring operation

![](img/preanaesthetic1.jpg)

**<span style="color:Black;font-size:1.2em">Intra Operation</span>**

<p align="justify">Intra operation begins when the patient is transferred to the operating room table and ends with the transfer of a patient to the postanesthesia care unit (PACU). During this period the patient is monitored, anesthetized, prepped, and draped, and the operation is performed.</p>

![](img/intraoperation.jpg)

Click record to record patients  details during the operation
![](img/intraoperation1.jpg)

**<span style="color:Black;font-size:1.2em">Recovery</span>**

<p align="justify">unit A suite in a hospital next to the surgical suites, where recently operated Pts are monitored for a period of hrs to ensure recovery from anesthesia, physiologic stress of surgery, prevent post-surgical complications–eg, aspiration and suffocation, identify arrhythmias, hypotension, and other conditions and/or acute decompensation of pre-existing conditions </p>

![](img/recovery.jpg)

**<span style="color:Black;font-size:1.2em">Post anaesthetic visit</span>** 

This sub setion deals with provision of immediate post-operative recovery for surgical patients and for other patients who have received sedation.

![](img/postanaestheticvisit.jpg)

**<span style="color:Black;font-size:1.2em">Complains</span>**

 This sub section gives the report of complains.

 ![](img/complains.jpg)

**<span style="color:Black;font-size:1.2em">Marriage Conflicts</span>**

This sub section gives the report describing marriage conflicts.

![](img/marriageconflicts.jpg)

<p align="justify">If the status is not completed, click view to complete the remaining details and for choosing the appropriate status according to the present situation.</p>

![](img/completedetails.jpg)

**<span style="color:Black;font-size:1.2em">MTUHA</span>**

MTUHA report gives data of Management System for Health Services.

![](img/socialmtuha.jpg)

![](img/socialmtuhaone.jpg)

**<span style="color:Black;font-size:1.5em">GBV/VAC</span>**

<p align="justify">Under GBV/VAC sub section the system allows user to search for violence that patient encounted, service is given, client violence effect is analysed, informant details are recorded and if there is any attachment,the attachment will be uploaded.</p>

GBV/VAC consist of ;

**<span style="color:Black;font-size:1.2em">Violence</span>**

To register the encountered violance;

1.Search violence type 
2. Enter date
3. Search violence category
4. Tick where appropriate
5. Save

![](img/violance.jpg)

**<span style="color:Black;font-size:1.2em">Services Given</span>**

The services given due to the encountered violence is entered under this sub section

![](img/servicegiven.jpg)

**<span style="color:Black;font-size:1.2em">Client Violance Effect</span>** 

Search and enter client violance effect.

![](img/clientviolenceeffect.jpg)

**<span style="color:Black;font-size:1.2em">Informant</span>**

Informant details are recorded under this sub section

![](img/informant.jpg)

**<span style="color:Black;font-size:1.2em">Attachment</span>**

All attachments concerning with the emerging violence together with the attachment description are attached under these sub section.

![](img/attachment.jpg)

**<span style="color:Black;font-size:1.2em">Marriage Issues</span>**

All issues raised in the marriage are reported and registered under this sub section

1. Search patient
2. Enter complainer description
3. Enter your description
4. specify time
5. Register

![](img/marriageissues.jpg)

**<span style="color:Black;font-size:1.2em">Change Patient Category</span>**

This is applicable for changing the category of a patient who was first registered as a payment patient but now that patient needs exemption.

To change patient category

1. Search patient
2. Enter exemption type
3. Enter exemption reason
4. Upload file (if there is any)
5. Register

![](img/changecategory.jpg)

**<span style="color:Black;font-size:1.5m">Hospital Shop</span>**

<p align="justify">The hospital shop is where medicine and medical supplies are sold. The hospital shop is used where the item does not exist in the hospital dispensing or when an outside customer comes out to buy something from the hospital shop.</p>
![](img/hospitalshop.jpg)

![](img/hospitalshop1.jpg)

**<span style="color:Black;font-size:1.5em">Financial Report</span>**

This part shows the report of financial report of each exemptions provided in a specified department

![](img/finance.jpg)

**<span style="color:Black;font-size:1.2em">Sub department finance</span>**  

Gives the report from sub departments

![](img/subdepartmentfinance.jpg)

**<span style="color:Black;font-size:1.5em">Detailed Finance</span>**

Gives the exemption detailed finance report

![](img/detailedfinance.jpg)


 

