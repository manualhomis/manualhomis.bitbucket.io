**<span style="color:Black;font-size:1.5em">WELCOME TO GoT-HoMISv3 DOCUMENTATION</span>**

![](img/"DEVELOPERS")

** <span style="color:Black;font-size:1.5em">General Information</span>**

<p align ="justify">The Government of the United Republic of Tanzania has acknowledged the importance of using technology in the management of health facilities which include Hospitals, Dispensaries and Health Centers to make them work more effectively and with high efficiency by changing the entire hospital process from using a lot of paper to paper less work. The use of technology will enable good time management when effecting the hospital management processes.</p>

<p align ="justify">The President’s Office – Regional Administration and Local Government(PO-RALG) in collaboration with the Ministry of Health, Community Development, Gender, Elderly and Children, Kibaha Education Center (KEC) and the University of Mzumbe have come up with a software system which is a solution for hospital management, named Government of Tanzania Hospital Management Information system(GoT-HoMIS).GoT-HoMIS was initiated by Kibaha Education Center (KEC) and Mzumbe University,it is being developed using local experts within the country.</br></br> Since its inception back in 2015. GoT-HoMIS has gone through version transitions and is now in version3. GoT-HoMIS is an electronic information system which incorporates various functionalities that play dynamic roles in line with Health Sector needs and the Government at large.</p>

The system incorporates various core functionalities/modules to serve the purpose of the system as follows.

**<span style="color:Black;font-size:1.2em">Electronic Medical Record (EMR) </span>**

<p align ="justify">Electronic Medical Record (EMR) comprises of patient Registration where patient details are recorded, Out-patient Department and In Patient Department record details of clients who visits the facility for various needs both in General and special clinics such as eye, teeth, TB, CTC, cardiac and other clinics where the records kept by the system includes records of patient previous visit, consultation, diagnosis, clerking, continuation notes, treatment details and investigation results. Once patient details are recorded, those details will be available every time a patient visits the facility and the information is useful to the doctors when they need the review patients previous visit records.</p>

**<span style="color:Black;font-size:1.2em">Laboratory Information System</span>**

<p align ="justify">Laboratory information system automatically transmits investigations requests and processes the delivery to the concerned department of the health facility. Doctor’s investigation requests and investigation results are automatically sent to relevant office for action.</p>

**<span style="color:Black;font-size:1.2em">Tracking  and Inventory of Medical Supplies</span>**

<p align ="justify">Tracking and inventory of medical supplies, the system provides functionality for requisition of medical supplies, purchase of items, stock management, automatic reorder level settings, online request for stock from main store to sub-store and dispensing points as they drop down to the final consumer</p>

**<span style="color:Black;font-size:1.2em">Billing and Revenue Collection</span>**

<p align ="justify">The system is also integrated with other systems such as; Government electronic Payment Gateway (GePG) where the system ensure electronic payment through the use of Government Electronic Payment Gateway (GePG). A healthcare client can pay for Instant Payment via telephone networks or banking agent via a hospital charge window, this way makes the process of billing and revenue collection to be more effective.</p>

NHIF system enable the verification of customer cards and sending client claims.

<p align ="justify">District Health Information System (DHIS) synonym Mfumo wa Taarifa za Uendeshaji wa Huduma Za Afya (MTUHA) helps in the preparation of District reports. MTUHA reporting system uses mandatory information collected during registration process so that a report can be sent to MTUHA in line with eLMIS for RnR.</p>

Modules Includes;

<p align ="justify"> Registration, Billing, Dispensing, Pharmacy(Main & Sub), Laboratory, Radiology(Ultrasound & X-Ray), OPD Doctor, Eye, Dental, Theatre, IPD Doctor, RCH, Social Welfare, Orthopedic, Obstetrics and Gynecology, Mortuary, Hospital Shop, CTC, Physiotherapy, Casualty, Nursing Care, TB, Diabetes, Cardiology, Dematology, Psychiatric, Nutrition, ENT, Medical, Pediatric, Mathedone, Urology, Blood Bank</p>

The presence of GoT-HoMIS in the process of health management has various advantages including;

<p align ="justify">The system tracks the flow of medicine from vendor to the final consumer. This makes easy to the management in controlling inventory and make a good record on how medicine is utilized in the facilities.</p>

<p align ="justify">– The system records all laboratory information  to enhance facilities to have a good record of disease/health disorder which is common in a particular area so that authorities can take precaution.</p>

<p align ="justify">– The system tracks billing and recording electronically which makes easy to control flow of money, in this way the management will be able to unnecessary loss of money.</p>

<p align ="justify">– The system may be used to generate MTUHA reports, departmental reports, performance reports, financial reports and so on.</p>

**<span style="color:Black;font-size:1.5em">Scope</span>**
<p align="justify">This manual applies to all health sectors such as regional hospital, district hospital, health centre and Dispensary.</p>
  
**<span style="color:Black;font-size:1.5em">System Overview</span>**
<p align="justify">The Government of Tanzania, Hospital Management Information System (GoT-HoMIS) is an electronic system for management and operation of health care facilities in Tanzania. system Collect and report facility level clinical information (basic patient level clinical dataset), and Support Health Facilities in service delivery management</p>



